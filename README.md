<!--
SPDX-License-Identifier: Unlicense
SPDX-FileCopyrightText: 2024 Vieno Hakkerinen <txt.file@txtfile.eu>
-->
# Role Name

Install and configure on zabbix FreeBSD.

## Requirements

FreeBSD machine

## Role Variables

none

## Dependencies

none

## Example Playbook

```
- hosts: localhost
  roles:
    - zabbix
```

## License

Unlicense
